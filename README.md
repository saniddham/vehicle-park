# README #


### What is this repository for? ###

* For parking management of vehicle

### How do I get set up? ###

* configure the db configurations and maxparking configuration on .env file
* run `npm install`
* run `npm start`


### sql queries need to insert ###

CREATE TABLE `vehicle_park`.`parking_slot` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `vehicle_park`.`parking` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `vehicleNo` VARCHAR(45) NOT NULL,
  `status` VARCHAR(45) NULL,
  `slotId` INT NOT NULL,
  PRIMARY KEY (`id`));
  
  ALTER TABLE `vehicle_park`.`parking_slot` 
  ADD COLUMN `status` INT NOT NULL AFTER `description`;
  
  NSERT INTO `vehicle_park`.`parking_slot` (`id`, `description`) VALUES ('1', '1st Slot');
  INSERT INTO `vehicle_park`.`parking_slot` (`id`, `description`) VALUES ('2', '2nd Slot');
  INSERT INTO `vehicle_park`.`parking_slot` (`id`, `description`) VALUES ('3', '3rd Slot');
  INSERT INTO `vehicle_park`.`parking_slot` (`id`, `description`) VALUES ('4', '4th Slot');
  INSERT INTO `vehicle_park`.`parking_slot` (`id`, `description`) VALUES ('5', '5th Slot');
  INSERT INTO `vehicle_park`.`parking_slot` (`id`, `description`) VALUES ('6', '6th Slot');
  INSERT INTO `vehicle_park`.`parking_slot` (`id`, `description`) VALUES ('7', '7th Slot');
  INSERT INTO `vehicle_park`.`parking_slot` (`id`, `description`) VALUES ('8', '8th Slot');
  INSERT INTO `vehicle_park`.`parking_slot` (`id`, `description`) VALUES ('9', '9th Slot');
  INSERT INTO `vehicle_park`.`parking_slot` (`id`, `description`) VALUES ('10', '10th Slot');

