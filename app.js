var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var sql = require('./sql');
var mysql = require('mysql');
var dotenv = require('dotenv');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

dotenv.config();
var connection, retryTimeout = `${process.env.retryMinTimeout}`;
function persistantConnection(){

  connection = mysql.createConnection({
      host: `${process.env.host}`,
      user: `${process.env.user}`,
      password: `${process.env.password}`,
      database: `${process.env.database}`
  });
  connection.connect(
      function (err){
        if (err){
          setTimeout(persistantConnection, retryTimeout);
          if (retryTimeout < `${process.env.retryMaxTimeout}`)
            retryTimeout += 1000;
        }
        else{
          retryTimeout = `${process.env.retryMinTimeout}`
        }
      });
  connection.on('error',
      function (err){
        if (err.code === 'PROTOCOL_CONNECTION_LOST')
          persistantConnection();
      });
}
persistantConnection();

app.use('/', indexRouter);
app.use('/users', usersRouter);

//get parking information
app.get('/parking', function (req, res) {

    var type = req.query.type;
    var parameter = req.query.parameter;
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    res.setHeader('Content-Type', 'application/json');
    if (parameter != undefined && parameter != '' && type != undefined && type != ''){
        if (type == 'slot') {
            connection.query(sql.getVehicleBookingInformation() + '\'' + parameter + '\'', function (err, rows, fields) {
                if (err) throw err

                if (rows.length > 0) {
                    var slotId = rows[0].id;
                    var vehicleNo = rows[0].vehicleNo;
                    res.end(JSON.stringify({ slotId: slotId, vehicleNo: vehicleNo }));
                } else {
                    res.end(JSON.stringify({ status: 'fail', message: 'could not find parking entries ' }));
                }
            })
        } else if (type == 'vehicle'){
            connection.query(sql.getSlotBookingInformation() + '\'' + parameter + '\'', function (err, rows, fields) {
                if (err) throw err
                if (rows.length > 0) {
                    var slotId = rows[0].id;
                    var vehicleNo = rows[0].vehicleNo;
                    res.end(JSON.stringify({ slotId: slotId, vehicleNo: vehicleNo }));
                } else {
                    res.end(JSON.stringify({ status: 'fail', message: 'could not find parking entries ' }));
                }
            })
        }
    } else {
        res.end(JSON.stringify({ status: 'fail', message: 'input parameters are not valid' }));
    }
})

//parking the vehicle api implementation

app.post('/park-vehicle', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    var parkingCount = 0;
    connection.query(sql.getParkingCount() , function (err, rows, fields) {
        if (err) throw err
        parkingCount = rows[0].parkingCount;
        connection.query(sql.getAvailableParkingSlots(), function (err, rows, fields) {
            if (err) {
                throw err
                res.end(JSON.stringify({ status: 'fail', message: 'data base connection error' }));
            }


            if (rows.length == 0 || `${process.env.maxParkingCount}` <= parkingCount ) {
                res.end(JSON.stringify({ status: 'fail', message: 'parking slots are unavailable' }));
            } else {
                var slotId = rows[0].id;
                var parkingEntry = {
                    vehicleNo: req.body.vehicle_no,
                    status: 1,
                    slotId: slotId
                };
                connection.query(
                    'INSERT INTO parking SET ?', parkingEntry, function (err, result) {
                        if (result){
                            connection.query(
                                'UPDATE parking_slot SET status = ? WHERE id = ? ',
                                [1 , slotId],
                                function(err, rows, fields){
                                    if (!err){
                                        res.end(JSON.stringify({ status: 'success', sloId: slotId}));
                                    } else {
                                        res.end(JSON.stringify({ status: 'fail', message: 'Error occured in updating the data' }));
                                    }
                                });
                        } else {
                            res.end(JSON.stringify({ status: 'fail', message: 'Error occured in inserting the data' }));
                        }
                    });
            }
        })

    })
})

//unparking the slot api implementation

app.post('/unpark-vehicle', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    var slotId = req.body.slotId;
    if (slotId != undefined && slotId != ''){
        connection.query(
            'UPDATE parking_slot SET status = ? WHERE id = ? ',
            [0 , slotId],
            function(err, rows, fields){
                if (!err){
                    connection.query(
                        'UPDATE parking SET status = ? WHERE id = ? AND status = ? ',
                        [0 , slotId, 1],
                        function(err, rows, fields){
                            if (!err){
                                res.end(JSON.stringify({ status: 'success', message: 'unparked the slot successfully', slotId: slotId }));
                            } else {
                                res.end(JSON.stringify({ status: 'fail', message: 'unparking the slot is unsuccessfull' }));
                            }
                        });
                    res.end(JSON.stringify({ status: 'success', message: 'unparked the slot successfully', slotId: slotId }));
                } else {
                    res.end(JSON.stringify({ status: 'fail', message: 'unparking the slot is unsuccessfull' }));
                }
            });
    } else {
        res.end(JSON.stringify({ status: 'fail', message: 'slot id is not valid' }));
    }
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
