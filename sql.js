
var sql = module.exports = {

    getAvailableParkingSlots: function() {
        return 'SELECT * FROM parking_slot WHERE status = 0 order by id limit 1';
    },

    getSlotBookingInformation: function(){
        return 'SELECT * FROM parking WHERE status = 1 AND vehicleNo = ';
    },

    getVehicleBookingInformation: function(){
        return 'SELECT * FROM parking WHERE status = 1 AND slotId =  ';
    },

    getParkingCount: function(){
        return 'SELECT COUNT(*) as parkingCount FROM parking WHERE status = 1 ';
    }

}
